package use_case

import (
	"github.com/Budi721/study-case-go/domain"
	"github.com/Budi721/study-case-go/infrastructure/repository"
)

type ProductUseCase interface {
	AddProduct(product *domain.ProductDTO) (*domain.Product, error)
	GetDetailProduct(id int) (*domain.Product, error)
	GetAllProduct() (*[]domain.Product, error)
	DeleteProduct(id int) error
	ChangeProduct(product *domain.ProductDTO, id int) (*domain.Product, error)
}

type ProductUseCaseImpl struct {
	repository.ProductRepo
}

func (p ProductUseCaseImpl) DeleteProduct(id int) error {
	return p.ProductRepo.DeleteProduct(id)
}

func (p ProductUseCaseImpl) AddProduct(product *domain.ProductDTO) (*domain.Product, error) {
	return p.ProductRepo.CreateProduct(product)
}

func (p ProductUseCaseImpl) GetDetailProduct(id int) (*domain.Product, error) {
	return p.ProductRepo.FindProductById(id)
}

func (p ProductUseCaseImpl) GetAllProduct() (*[]domain.Product, error) {
	return p.ProductRepo.FindProducts()
}

func (p ProductUseCaseImpl) ChangeProduct(product *domain.ProductDTO, id int) (*domain.Product, error) {
	return p.ProductRepo.UpdateProduct(product, id)
}

func NewProductUseCase(productRepo repository.ProductRepo) ProductUseCase {
	return &ProductUseCaseImpl{ProductRepo: productRepo}
}
