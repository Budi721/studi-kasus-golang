package domain

type Product struct {
	Name   string  `json:"name,omitempty"`
	Rating float32 `json:"rating,omitempty"`
	Id     int     `json:"id,omitempty"`
	Harga  int     `json:"harga,omitempty"`
	Likes  int     `json:"likes,omitempty"`
}

type ProductDTO struct {
	Name   string  `json:"name,omitempty"`
	Rating float32 `json:"rating,omitempty"`
	Harga  int     `json:"harga,omitempty"`
	Likes  int     `json:"likes,omitempty"`
}

type ProductSortingImpl []Product

func (p ProductSortingImpl) Len() int {
	return len(p)
}

func (p ProductSortingImpl) Less(i, j int) bool {
	// harga terendah
	if p[i].Harga != p[j].Harga {
		return p[i].Harga < p[j].Harga
	}
	// harga sama maka urutkan berdasarkan rating tertinggi
	if p[i].Rating != p[j].Rating {
		return p[i].Rating > p[j].Rating
	}
	// jika rating sama maka urutkan berdasarkan likes terbanyak.
	return p[i].Likes > p[j].Likes
}

func (p ProductSortingImpl) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
