package database

import (
	"github.com/joho/godotenv"
	"gorm.io/gorm"
	"log"
	"os"
)

type Config struct {
	DBUsername string
	DBPassword string
	DBHost     string
	DBPort     string
	DBName     string
}

func Init() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println("gagal load env variabel")
	}

	return &Config{
		DBUsername: os.Getenv("DB_USERNAME"),
		DBPassword: os.Getenv("DB_PASSWORD"),
		DBHost:     os.Getenv("DB_HOST"),
		DBPort:     os.Getenv("DB_PORT"),
		DBName:     os.Getenv("DB_NAME"),
	}
}

func InitGorm() *gorm.DB {
	mysqlClient := NewMysqlClient(ClientConfig{
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		DBName:   os.Getenv("DB_NAME"),
	})

	return mysqlClient.OpenDB()
}
