package database

import (
	"fmt"
	"github.com/Budi721/study-case-go/domain"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

type ClientConfig struct {
	Username string
	Password string
	Host     string
	Port     string
	DBName   string
}

type Client interface {
	Ping() error
	OpenDB() *gorm.DB
}

type client struct {
	dbConnection *gorm.DB
}

func (c *client) Ping() error {
	var result int64
	tx := c.dbConnection.Raw("select 1").Scan(&result)
	if tx.Error != nil {
		return fmt.Errorf("mysql unable to serve basic query. %v", tx.Error)
	}
	return nil
}

func (c *client) OpenDB() *gorm.DB {
	return c.dbConnection
}

func NewMysqlClient(config ClientConfig) Client {
	connection := fmt.Sprintf("%s:%s@tcp(%s:%v)/%s?charset=utf8mb4&parseTime=True&loc=UTC",
		config.Username,
		config.Password,
		config.Host,
		config.Port,
		config.DBName,
	)

	dbConnection, err := gorm.Open(mysql.Open(connection), &gorm.Config{
		SkipDefaultTransaction:                   true,
		PrepareStmt:                              true,
		DisableForeignKeyConstraintWhenMigrating: true,
	})

	if err != nil {
		log.Fatalf("unable to initiate mysql connection. %v", err)
	}

	err = dbConnection.AutoMigrate(&domain.Product{})
	if err != nil {
		log.Fatalf("unable to migrate db. %v", err)
	}

	return &client{
		dbConnection: dbConnection,
	}
}
