package http

import (
	"github.com/Budi721/study-case-go/infrastructure/database"
	"gorm.io/gorm"
)

type Application struct {
	Config *database.Config
	DB     *gorm.DB
}

func Init() *Application {
	application := Application{
		Config: database.Init(),
		DB:     database.InitGorm(),
	}

	return &application
}
