package repository

import (
	"github.com/Budi721/study-case-go/domain"
	"gorm.io/gorm"
)

type ProductRepo interface {
	FindProducts() (*[]domain.Product, error)
	FindProductById(id int) (*domain.Product, error)
	CreateProduct(product *domain.ProductDTO) (*domain.Product, error)
	UpdateProduct(product *domain.ProductDTO, id int) (*domain.Product, error)
	DeleteProduct(id int) error
}

type ProductRepoImpl struct {
	*gorm.DB
}

func (p *ProductRepoImpl) FindProducts() (*[]domain.Product, error) {
	var products []domain.Product
	p.DB.Find(&products)
	return &products, nil
}

func (p *ProductRepoImpl) FindProductById(id int) (*domain.Product, error) {
	var product domain.Product
	p.DB.Take(&product, &domain.Product{Id: id})
	return &product, nil
}

func (p *ProductRepoImpl) CreateProduct(product *domain.ProductDTO) (*domain.Product, error) {
	productToInsert := domain.Product{
		Name:   product.Name,
		Rating: product.Rating,
		Harga:  product.Harga,
		Likes:  product.Likes,
	}

	p.DB.Create(&productToInsert)
	return &productToInsert, nil
}

func (p *ProductRepoImpl) UpdateProduct(product *domain.ProductDTO, id int) (*domain.Product, error) {
	var productToEdit domain.Product
	p.DB.First(&productToEdit, &domain.Product{Id: id})
	productToEdit.Harga = product.Harga
	productToEdit.Likes = product.Likes
	productToEdit.Name = product.Name
	productToEdit.Rating = product.Rating
	p.DB.Save(&productToEdit)
	return &productToEdit, nil
}

func (p *ProductRepoImpl) DeleteProduct(id int) error {
	p.DB.Delete(&domain.Product{}, id)
	return nil
}

func NewProductRepository(DB *gorm.DB) ProductRepo {
	return &ProductRepoImpl{DB: DB}
}
