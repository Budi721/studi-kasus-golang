//go:build wireinject
// +build wireinject

package inject

import (
	"github.com/Budi721/study-case-go/application/use_case"
	"github.com/Budi721/study-case-go/infrastructure/http"
	"github.com/Budi721/study-case-go/infrastructure/repository"
	"github.com/Budi721/study-case-go/interface/http/api"
	"github.com/google/wire"
)

func InitializeProductHandler() api.ProductHandler {
	wire.Build(
		api.NewProductHandler,
		use_case.NewProductUseCase,
		repository.NewProductRepository,
		http.Init,
		wire.FieldsOf(new(*http.Application), "DB"),
	)

	return nil
}
