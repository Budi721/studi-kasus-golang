package api

import (
	"encoding/json"
	"github.com/Budi721/study-case-go/application/use_case"
	"github.com/Budi721/study-case-go/common/helper"
	"github.com/Budi721/study-case-go/domain"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type ProductHandler interface {
	DeleteProductHandler() func(http.ResponseWriter, *http.Request)
	PutProductHandler() func(http.ResponseWriter, *http.Request)
	PostProductHandler() func(http.ResponseWriter, *http.Request)
	GetProductDetailHandler() func(http.ResponseWriter, *http.Request)
	GetProductsHandler() func(http.ResponseWriter, *http.Request)
}

type ProductHandlerImpl struct {
	use_case.ProductUseCase
}

func (p *ProductHandlerImpl) DeleteProductHandler() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, _ := strconv.Atoi(vars["id"])
		err := p.ProductUseCase.DeleteProduct(id)
		if err != nil {
			helper.RespondWithError(writer, 500, err.Error())
		}
		helper.RespondWithJSON(writer, 200, map[string]string{
			"message": "sukses menghapus produk",
		})
	}
}

func (p *ProductHandlerImpl) PutProductHandler() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, _ := strconv.Atoi(vars["id"])
		var productRequest domain.ProductDTO
		decoder := json.NewDecoder(request.Body)
		if err := decoder.Decode(&productRequest); err != nil {
			helper.RespondWithError(writer, http.StatusBadRequest, "Invalid request payload")
			return
		}
		defer request.Body.Close()

		product, err := p.ChangeProduct(&productRequest, id)
		if err != nil {
			helper.RespondWithError(writer, 500, err.Error())
		}
		helper.RespondWithJSON(writer, 200, product)
	}
}

func (p *ProductHandlerImpl) PostProductHandler() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		println(request.Body)
		var productRequest domain.ProductDTO
		decoder := json.NewDecoder(request.Body)
		if err := decoder.Decode(&productRequest); err != nil {
			helper.RespondWithError(writer, http.StatusBadRequest, "Invalid request payload")
			return
		}
		defer request.Body.Close()

		product, err := p.ProductUseCase.AddProduct(&productRequest)
		if err != nil {
			helper.RespondWithError(writer, 500, err.Error())
		}
		helper.RespondWithJSON(writer, 200, product)
	}
}

func (p *ProductHandlerImpl) GetProductDetailHandler() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		vars := mux.Vars(request)
		id, _ := strconv.Atoi(vars["id"])
		product, err := p.ProductUseCase.GetDetailProduct(id)
		if err != nil {
			helper.RespondWithError(writer, 500, err.Error())
		}
		helper.RespondWithJSON(writer, 200, product)
	}
}

func (p *ProductHandlerImpl) GetProductsHandler() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		product, err := p.ProductUseCase.GetAllProduct()
		if err != nil {
			helper.RespondWithError(writer, 500, err.Error())
		}
		helper.RespondWithJSON(writer, 200, product)
	}
}

func NewProductHandler(productUseCase use_case.ProductUseCase) ProductHandler {
	return &ProductHandlerImpl{ProductUseCase: productUseCase}
}
