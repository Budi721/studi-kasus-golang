package api

import (
	"github.com/Budi721/study-case-go/inject"
	"github.com/gorilla/mux"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter()
	productHandler := inject.InitializeProductHandler()
	router.HandleFunc("/products", productHandler.GetProductsHandler()).Methods("GET")
	router.HandleFunc("/products/{id}", productHandler.GetProductDetailHandler()).Methods("GET")
	router.HandleFunc("/products", productHandler.PostProductHandler()).Methods("POST")
	router.HandleFunc("/products/{id}", productHandler.PutProductHandler()).Methods("PUT")
	router.HandleFunc("/products/{id}", productHandler.DeleteProductHandler()).Methods("DELETE")
	return router
}
