package main

import (
	"bufio"
	"errors"
	"fmt"
	"github.com/Budi721/study-case-go/domain"
	"github.com/Budi721/study-case-go/infrastructure/http"
	"github.com/Budi721/study-case-go/interface/http/api"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	// untuk problem no 1
	var products domain.ProductSortingImpl
	products = []domain.Product{
		{
			Name:   "Indomie",
			Rating: 5,
			Harga:  3000,
			Likes:  150,
		},
		{
			Name:   "Laptop",
			Rating: 4.5,
			Harga:  4000000,
			Likes:  123,
		},
		{
			Name:   "Aqua",
			Rating: 4,
			Harga:  3000,
			Likes:  250,
		},
		{
			Name:   "Smart TV",
			Rating: 4.5,
			Harga:  4000000,
			Likes:  42,
		},
		{
			Name:   "Headphone",
			Rating: 3.5,
			Harga:  4000000,
			Likes:  90,
		},
		{
			Name:   "Very Smart TV",
			Rating: 3.5,
			Harga:  4000000,
			Likes:  87,
		},
	}

	sort.Sort(products)
	for _, p := range products {
		println(p.Name)
	}

	// untuk problem no 3
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Masukan angka contoh (5 * 5): ")
	text, _ := reader.ReadString('\n')
	result, err := calculator(text)
	fmt.Println(result)
	fmt.Println(err)

	// untuk problem no 2 run server
	c := http.NewCli(os.Args)
	c.Run(api.NewRouter())
}

func calculator(inputUser string) (int, error) {
	words := strings.Fields(inputUser)
	firstOperand, err := strconv.Atoi(words[0])
	if err != nil {
		return -1, err
	}
	secondOperand, err := strconv.Atoi(words[2])
	if err != nil {
		return -1, err
	}

	if firstOperand > 1000000 || secondOperand > 1000000 {
		return -1, errors.New("nilai operand maksimal 1000000")
	}

	switch words[1] {
	case "+":
		return firstOperand + secondOperand, nil
	case "-":
		return firstOperand - secondOperand, nil
	case "*":
		return firstOperand * secondOperand, nil
	case "/":
		return firstOperand / secondOperand, nil
	default:
		return -1, errors.New("tidak ada operator yang tepat")
	}

}
